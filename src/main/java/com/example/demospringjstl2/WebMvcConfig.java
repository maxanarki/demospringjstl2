package com.example.demospringjstl2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

@Configuration
//@EnableWebMvc
public class WebMvcConfig //extends WebMvcConfigurationSupport
{

//    public WebMvcConfig() {
//        super();
//    } //WebMvcConfigurerAdapter

//    @Bean
//    public ViewResolver getDocxViewResolver() {
//        UrlBasedViewResolver basedViewResolver = new UrlBasedViewResolver();
//        basedViewResolver.setPrefix("/doc/"); // /WEB-INF/views/
//        basedViewResolver.setSuffix(".docx");
//        basedViewResolver.setOrder(1);
//        basedViewResolver.setViewClass(DocxView.class);
//        return basedViewResolver;
//    }

//    @Bean
//    public ViewResolver getViewResolver() {
//        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
//        resolver.setPrefix("/"); // /WEB-INF/views/
//        resolver.setSuffix(".jsp");
//        resolver.setOrder(2_147_483_643); //max: 2147483647 (magic number: 2_147_483_642)
//        //resolver.setViewClass(.class);
//        return resolver;
//    }

//    @Bean
//    public ViewResolver getViewResolver2() {
//        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
//        //resolver.setPrefix("/"); // /WEB-INF/views/
//        //resolver.setSuffix(".html");
//        resolver.setOrder(0);
//        //resolver.setViewClass(Thyme.class);
//        return resolver;
//    }

//    @Override
//    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
//        configurer.enable();
//    }

//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/**").addResourceLocations("/");//WEB-INF/resources/
//    }

//    @Override
//    protected void configureViewResolvers(ViewResolverRegistry registry) {
//        super.configureViewResolvers(registry);
//    }
}




//@Configuration
////@EnableWebMvc
//public class WebMvcConfig
//                        implements WebMvcConfigurer
//                        //extends WebMvcConfigurationSupport
//                        //extends DelegatingWebMvcConfiguration
//                        {

//    @Bean(name="simpleMappingExceptionResolver")
//    public SimpleMappingExceptionResolver createSimpleMappingExceptionResolver()
//    {
//        SimpleMappingExceptionResolver r = new SimpleMappingExceptionResolver();
//
//        Properties mappings = new Properties();
//        mappings.setProperty("DatabaseException", "databaseError");
//        mappings.setProperty("NoHandlerFoundException", "noHandlerFoundEx");
//        mappings.setProperty("ResourceNotFoundException", "resourceNotFoundException");
//
//        r.setExceptionMappings(mappings);  // None by default
//        //r.setDefaultErrorView("error");    // No default
//        r.setExceptionAttribute("exx");     // Default is "exception"
//        //r.setWarnLogCategory("example.MvcLogger");     // No default
//        return r;
//    }

//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        //super.addResourceHandlers(registry);
//
//        System.out.println("WebMvcConfig: addResourceHandlers:");
//
//        registry.addResourceHandler(//"/**/*.html",
//                            "/**/*.css", "/**/*.css.map", "/**/*.js", "/**/*.ico")
//                .addResourceLocations("/", "qqq",
//                        //"WEB-INF",
//                        "css",
//                        "js",
//                        "WEB-INF/resources",
//                        "qqq/www");//, "/WEB-INF/"
//
////        registry
////                .addResourceHandler("/js/**")
////                .addResourceLocations("/js/")
////                .setCachePeriod(3600)
////                .resourceChain(true);
//
//    }

//        @Override
//        public void extendHandlerExceptionResolvers(List<HandlerExceptionResolver> resolvers) {
//                                // Spring MVC is not enabled by default SimpleMappingExceptionResolver, so there need to manually add
//                                // SimpleMappingExceptionResolver
//                                SimpleMappingExceptionResolver simpleMappingExceptionResolver=new SimpleMappingExceptionResolver();
//
//                                // sets the exception / error mapping table view name
//                                Properties mappingsExceptionsToViewName = new Properties();
//                                mappingsExceptionsToViewName.setProperty(SimpleMappingExceptionResolver.class.getName(),"error2");
//                                mappingsExceptionsToViewName.setProperty("com.example.demo.ResourceNotFoundException", "resourceNotFoundException");
//                                mappingsExceptionsToViewName.setProperty("NoHandlerFoundException", "noHandlerFoundEx");
//
//                                simpleMappingExceptionResolver.setExceptionMappings(mappingsExceptionsToViewName);
//
//                                // set the error view name / HTTP error code mapping table
//                                Properties mappingsViewNameToStatusCode=new Properties();
//                                mappingsViewNameToStatusCode.setProperty("error2","408");
//                                simpleMappingExceptionResolver.setStatusCodes(mappingsViewNameToStatusCode);
//
//
////            SimpleMappingExceptionResolver r = new SimpleMappingExceptionResolver();
////
////            Properties mappings = new Properties();
////            mappings.setProperty("DatabaseException", "databaseError");
////            mappings.setProperty("NoHandlerFoundException", "noHandlerFoundEx");
////            mappings.setProperty("ResourceNotFoundException", "resourceNotFoundException");
////
////            r.setExceptionMappings(mappings);  // None by default
////            r.setDefaultErrorView("error");    // No default
////            r.setExceptionAttribute("exx");     // Default is "exception"
////            //r.setWarnLogCategory("example.MvcLogger");     // No default
////            //return r;
//
//            resolvers.add(simpleMappingExceptionResolver);
//        }
//}