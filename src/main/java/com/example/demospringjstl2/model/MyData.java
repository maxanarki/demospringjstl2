package com.example.demospringjstl2.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Valid
@Getter
public class MyData
{
    public MyData(String msg, int c) {
        this.message = msg;
        this.count = c;
    }

    @Size(min = 4, max = 7)
    String message;

    @Min(10)
    int count;
}

