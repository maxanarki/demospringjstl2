package com.example.demospringjstl2.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;

//@Valid
@Validated
@RestController
class ThisWillActuallyRunRestController {

    @GetMapping("/{message}/rest")
    public String home(@ModelAttribute @PathVariable("message") @Size(min = 4, max = 7) String msg
                      //@ModelAttribute @RequestParam("message") @Size(min = 4, max = 7) String msg
    ) {
        return "RestController: " + msg;
    }

}