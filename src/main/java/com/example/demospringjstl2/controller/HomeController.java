package com.example.demospringjstl2.controller;


import com.example.demospringjstl2.model.MyData;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebParam;
import javax.validation.Valid;
import javax.validation.constraints.Size;


@Validated
@Controller
public class HomeController
{

    @GetMapping({"", "/", "/index1"})
    public String index(Model model) {
        model.addAttribute("message", "messagemessagemessage");
        return "index.html";
    }


    @GetMapping({"iii"}) //{message}
    public String iii(Model model
                      //@ModelAttribute @PathVariable("message") @Size(min = 4, max = 7) String msg
                      ,@ModelAttribute(value = "message", binding = true) @Size(min = 4, max = 7) String msg ) { //@RequestParam("message")
        //model.addAttribute("message", msg);
        return "index.jsp";
    }


    @RequestMapping("/body")
    @ResponseBody
    public String home(@Valid @ModelAttribute("msg") MyData o
                       //@RequestParam("message") @Size(min = 5, max = 10) String msg
                       , Model model
                       ) {
        return String.format("rbody hello: '%s', '%d'", o.getMessage(), o.getCount());
    }


    @RequestMapping(value = "/doc", method = RequestMethod.GET)
    public String doGenerate(Model model) {
        model.addAttribute("filename", "maxim.docx");
        model.addAttribute("test", "test");
        return "maxim";
    }
}
